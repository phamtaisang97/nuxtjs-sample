import _set from 'lodash/set'
export const state = () => ({
  sidebar: {
    isCollapse: false,
    width: 192
  }
})
export const mutations = {
  SET_SIDE_BAR_COLLAPPSE(state, value) {
    state.sidebar.isCollapse = value
    state.sidebar.width = value ? 64 : 192
  },
  SET_PROPERTY_AUTH_USER(state, { key, value }) {
    _set(state.auth.user, key, value)
  }
}
