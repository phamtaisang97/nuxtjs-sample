import { set, add, update, remove, setProperty } from '@/utils/vuex'
const paginationDefault = {
  total: 0,
  pages: 1,
  limit: 20,
  page: 1
}
export const state = () => ({
  listGroupCustomer: [],
  selectGroupCustomer: {},
  api: '/api/group-customer/',
  pagination: paginationDefault
})

export const getters = {}
export const mutations = {
  SET_LIST_GROUP_CUSTOMER: set('listGroupCustomer', []),
  ADD_LIST_GROUP_CUSTOMER: add('listGroupCustomer'),
  DELETE_LIST_GROUP_CUSTOMER: remove('listGroupCustomer'),
  SELECT_GROUP_CUSTOMER: set('selectGroupCustomer', {}),
  UPDATE_LIST_GROUP_CUSTOMER: update('listGroupCustomer'),
  SET_PROPERTY_ACTIVE: setProperty('listGroupCustomer'),
  SET_PAGINATION(state, value) {
    Object.assign(state.pagination, value)
  }
}

export const actions = {
  async getListGroupCustomer({ commit, state }, { page = 1 }) {
    const res = { isSuccess: false }
    try {
      const { items, meta } = await this.$axios.$get(state.api, {
        params: {
          limit: state.pagination.limit,
          page,
          sort: '-createdAt'
        }
      })
      commit('SET_LIST_GROUP_CUSTOMER', items)
      commit('SET_PAGINATION', meta)
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async getIdGroupCustomer({ commit, state }, { id }) {
    const res = { isSuccess: false }
    try {
      const item = await this.$axios.$get(`${state.api}${id}`)
      commit('SELECT_GROUP_CUSTOMER', item)
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async updateGroupCustomer({ commit, state }, { id, data }) {
    const res = { isSuccess: false }
    try {
      const groupCustomer = await this.$axios.$put(`${state.api}${id}`, data)
      commit('UPDATE_LIST_GROUP_CUSTOMER', { value: groupCustomer })
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async createGroupCustomer({ commit, state }, { data }) {
    const res = { isSuccess: false }
    try {
      const groupCustomer = await this.$axios.$post(state.api, data)
      commit('ADD_LIST_GROUP_CUSTOMER', {
        newEl: groupCustomer,
        limit: state.pagination.limit
      })
      res.isSuccess = true
      res.data = groupCustomer
    } catch (error) {}
    return res
  },
  async deleteGroupCustomer({ commit, state }, { id }) {
    const res = { isSuccess: false }
    try {
      await this.$axios.$delete(`${state.api}${id}`)
      commit('DELETE_LIST_GROUP_CUSTOMER', { id })
      res.isSuccess = true
    } catch (error) {}
    return res
  }
}
