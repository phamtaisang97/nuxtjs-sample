import {set, add, update, remove, setProperty } from '@/utils/vuex'
const paginationDefault = {
    total: 0,
    pages: 1,
    limit: 20,
    page: 1
}
export const state = () => ({
    listUser: [],
    selectUser: {},
    api: '/',
    pagination: paginationDefault
})

export const getters = {}
export const mutations = {
    SET_LIST_USER: set('listUser', []),
    ADD_LIST_USER: add('listUser'),
    DELETE_LIST_USER: remove('listUser'),
    SELECT_USER: set('selectUser', {}),
    UPDATE_LIST_USER: update('listUser'),
    SET_PROPERTY_ACTIVE: setProperty('listUser'),
    SET_PAGINATION(state, value) {
        Object.assign(state.pagination, value)
    }
}

export const actions = {
    async getListUser({ commit, state }, query) {
        const res = { isSuccess: false }
        try {
            const { items, meta } = await this.$axios.$get(state.api, {
                params: {
                    limit: state.pagination.limit,
                    sort: '-createdAt',
                    ...query
                }
            })
            commit('SET_LIST_USER', items)
            commit('SET_PAGINATION', meta)
            res.isSuccess = true
        } catch (error) {}
        return res
    },
    async updateUser({ commit, state }, { id, data }) {
        const res = { isSuccess: false }
        try {
            const user = await this.$axios.$put(`${state.api}${id}`, data)
            commit('UPDATE_LIST_USER', { value: user })
            res.isSuccess = true
        } catch (error) {}
        return res
    },
    async register({ state }, { data }) {
        const res = { isSuccess: false }
        try {
            await this.$axios.$post(`${state.api}register`, data)
            res.isSuccess = true
        } catch (error) {}
        return res
    },
    async deleteUser({ commit, state }, { id }) {
        const res = { isSuccess: false }
        try {
            await this.$axios.$delete(`${state.api}${id}`)
            commit('DELETE_LIST_USER', { id })
            res.isSuccess = true
        } catch (error) {}
        return res
    }
}