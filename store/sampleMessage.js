import { set, add, update, remove, setProperty } from '@/utils/vuex'
const paginationDefault = {
  total: 0,
  pages: 1,
  limit: 20,
  page: 1
}
export const state = () => ({
  listSampleMessage: [],
  selectSampleMessage: {},
  api: '/api/sample-message/',
  pagination: paginationDefault
})

export const getters = {}
export const mutations = {
  SET_LIST_SAMPLE_MESSAGE: set('listSampleMessage', []),
  ADD_LIST_SAMPLE_MESSAGE: add('listSampleMessage'),
  DELETE_LIST_SAMPLE_MESSAGE: remove('listSampleMessage'),
  SELECT_SAMPLE_MESSAGE: set('selectSampleMessage', {}),
  UPDATE_LIST_SAMPLE_MESSAGE: update('listSampleMessage'),
  SET_PROPERTY_ACTIVE: setProperty('listSampleMessage'),
  SET_PAGINATION(state, value) {
    Object.assign(state.pagination, value)
  }
}

export const actions = {
  async getListSampleMessage({ commit, state }, query) {
    const res = { isSuccess: false }
    try {
      const { items, meta } = await this.$axios.$get(state.api, {
        params: {
          limit: state.pagination.limit,
          sort: 'pos,-createdAt',
          ...query
        }
      })
      commit('SET_LIST_SAMPLE_MESSAGE', items)
      commit('SET_PAGINATION', meta)
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async updateSampleMessage({ commit, state }, { id, data }) {
    const res = { isSuccess: false }
    try {
      const sampleMessage = await this.$axios.$put(`${state.api}${id}`, data)
      commit('UPDATE_LIST_SAMPLE_MESSAGE', { value: sampleMessage })
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async updatePos({ state }, { id, data }) {
    const res = { isSuccess: false }
    try {
      await this.$axios.$put(`${state.api}${id}/pos`, data)
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async createSampleMessage({ commit, state }, { data }) {
    const res = { isSuccess: false }
    try {
      const sampleMessage = await this.$axios.$post(state.api, data)
      commit('ADD_LIST_SAMPLE_MESSAGE', {
        newEl: sampleMessage,
        limit: state.pagination.limit
      })
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async deleteSampleMessage({ commit, state }, { id }) {
    const res = { isSuccess: false }
    try {
      await this.$axios.$delete(`${state.api}${id}`)
      commit('DELETE_LIST_SAMPLE_MESSAGE', { id })
      res.isSuccess = true
    } catch (error) {}
    return res
  }
}
