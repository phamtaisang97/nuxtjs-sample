import { set, add, update, remove, setProperty } from '@/utils/vuex'
const paginationDefault = {
  total: 0,
  pages: 1,
  limit: 20,
  page: 1
}
export const state = () => ({
  listCustomer: [],
  selectCustomer: {},
  api: '/api/customer/',
  pagination: paginationDefault
})

export const getters = {}
export const mutations = {
  SET_LIST_CUSTOMER: set('listCustomer', []),
  ADD_LIST_CUSTOMER: add('listCustomer'),
  DELETE_LIST_CUSTOMER: remove('listCustomer'),
  SELECT_CUSTOMER: set('selectCustomer', {}),
  UPDATE_LIST_CUSTOMER: update('listCustomer'),
  SET_PROPERTY_ACTIVE: setProperty('listCustomer'),
  SET_PAGINATION(state, value) {
    Object.assign(state.pagination, value)
  }
}

export const actions = {
  async getListCustomer({ commit, state }, query) {
    const res = { isSuccess: false }
    try {
      const { items, meta } = await this.$axios.$get(state.api, {
        params: {
          limit: state.pagination.limit,
          sort: '-createdAt',
          ...query
        }
      })
      commit('SET_LIST_CUSTOMER', items)
      commit('SET_PAGINATION', meta)
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async updateCustomer({ commit, state }, { id, data }) {
    const res = { isSuccess: false }
    try {
      const customer = await this.$axios.$put(`${state.api}${id}`, data)
      commit('UPDATE_LIST_CUSTOMER', { value: customer })
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async createCustomer({ commit, state }, { data }) {
    const res = { isSuccess: false }
    try {
      const customer = await this.$axios.$post(state.api, data)
      commit('ADD_LIST_CUSTOMER', {
        newEl: customer,
        limit: state.pagination.limit
      })
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async uploadFile({ state }, params) {
    const res = { isSuccess: false }
    try {
      const data = await this.$axios.$post(`${state.api}upload-file`, params)
      res.isSuccess = true
      res.data = data
    } catch (error) {}
    return res
  },
  async deleteCustomer({ commit, state }, { id }) {
    const res = { isSuccess: false }
    try {
      await this.$axios.$delete(`${state.api}${id}`)
      commit('DELETE_LIST_CUSTOMER', { id })
      res.isSuccess = true
    } catch (error) {}
    return res
  }
}
