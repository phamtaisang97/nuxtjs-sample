import { set } from '@/utils/vuex'
export const state = () => ({
  selectEditor: {},
  api: '/api/editor/'
})

export const getters = {}
export const mutations = {
  SELECT_EDITOR: set('selectEditor', {})
}

export const actions = {
  async getEditor({ commit, state }, { slug }) {
    const res = { isSuccess: false }
    try {
      const editor = await this.$axios.$get(`${state.api}${slug}`)
      commit('SELECT_EDITOR', editor)
      res.isSuccess = true
      res.data = editor
    } catch (error) {}
    return res
  },
  async createEditor({ commit, state }, { data }) {
    const res = { isSuccess: false }
    try {
      const editor = await this.$axios.$post(state.api, data)
      commit('SELECT_EDITOR', editor)
      res.isSuccess = true
    } catch (error) {}
    return res
  },
  async uploadFile({ state }, params) {
    const res = { isSuccess: false }
    try {
      const data = await this.$axios.$post(`/api/upload`, params)
      res.isSuccess = true
      res.data = data
    } catch (error) {}
    return res
  }
}
