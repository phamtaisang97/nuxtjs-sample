import Vue from 'vue'
import { mapState } from 'vuex'
import { formatDate } from '@/utils/format'
Vue.mixin({
  filters: {
    renderDateTime(val, format = 'h:mm dd/MM/YY') {
      return formatDate(val, format)
    },
    toLocaleString(val) {
      if (val) return val.toLocaleString('vi-VN')
      return val
    },
    checkNull(val) {
      if (val) return val
      return 'Chưa cập nhật'
    }
  },
  computed: {
    ...mapState('auth', ['user'])
  }
})
