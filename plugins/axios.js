import { Notification } from 'element-ui'

export default function({ $axios, app, redirect }) {
  $axios.onError((error) => {
    if (error.response.status === 401) {
      try {
        Notification.warning({
          title: 'Thông báo',
          message:
            error.response?.data?.message ||
            'Thông tin đăng nhập chưa chính xác',
          position: 'bottom-right'
        })
        if (process.client && error.response?.data?.type === 'USER_NOT_FOUND') {
          app.$auth.setToken('local', null)
          redirect('/login')
        }
      } catch (err) {
        console.log(err)
      }
    } else if (error.response.status === 413) {
      Notification.warning({
        title: 'Thông báo',
        message: 'Dung lượng file upload quá lớn, vui lòng kiểm tra lại',
        position: 'bottom-right'
      })
    } else if (process.client && error.response.data instanceof Blob) {
      const reader = new FileReader()
      reader.onload = function() {
        Notification.warning({
          title: 'Thông báo',
          message: JSON.parse(this.result).message || 'Lỗi kết nối',
          position: 'bottom-right'
        })
      }
      reader.readAsText(error.response.data)
    } else if (error.response.status === 422) {
      Notification.warning({
        title: 'Thông báo',
        message:
          error.response.data.message || error.response.data.data[0].message,

        position: 'bottom-right'
      })
    } else {
      Notification.warning({
        title: 'Thông báo',
        message:
          error.response.data && error.response.data.message
            ? error.response.data.message
            : 'Lỗi kết nối',
        position: 'bottom-right'
      })
    }
    return Promise.reject(error)
  })
  $axios.onRequest(() => {})
}
