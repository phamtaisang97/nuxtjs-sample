import io from 'socket.io-client'
const socket = io(process.env.API_SOCKET)
export default socket
