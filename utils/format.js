const defaultFormatter = {
  d: date => date.getDate(),
  dd: date =>
    date
      .getDate()
      .toString()
      .padStart(2, "0"),
  h: date =>
    date
      .getHours()
      .toString()
      .padStart(2, "0"),
  mm: date =>
    date
      .getMinutes()
      .toString()
      .padStart(2, "0"),
  ss: date =>
    date
      .getSeconds()
      .toString()
      .padStart(2, "0"),
  MM: date => (date.getMonth() + 1).toString().padStart(2, "0"),
  Y: date => date.getFullYear(),
  YY: date => date.getFullYear() % 100,
  yyyy: date =>
    date
      .getFullYear()
      .toString()
      .padStart(2, "0")
};
export const formatDate = (date, format, formatter = defaultFormatter) => {
  date = new Date(date);
  // sort keys by length, then value
  const keys = Object.keys(formatter)
    .sort((a, b) => {
      if (a.length > b.length) return -1;
      if (a.length < b.length) return 1;
      if (a > b) return -1;
      if (a < b) return 1;
      return 0;
    })
    .join("|");
  return format.replace(new RegExp(`${keys}`, "g"), m => {
    return formatter[m](date);
  });
};
