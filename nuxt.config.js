require('dotenv').config()
export default {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#1461d2' },
    /*
     ** Global CSS
     */
    css: [
        'element-ui/lib/theme-chalk/index.css',
        '@/assets/editor.scss',
        '@/assets/custom.scss',
        '@/assets/customSidebar.scss',
        '@/static/font-awesome/css/all.css'
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '@/plugins/socket.io.js',
        '@/plugins/element-ui',
        '@/plugins/axios',
        { src: '@/plugins/vue2Editor', mode: 'client' },
        '@/plugins/mixins/constant.js'
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/eslint-module'
    ],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        // Doc: https://github.com/nuxt-community/dotenv-module
        '@nuxtjs/dotenv'
    ],
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {
        proxy: 'true'
    },

    proxy: {
        '/api/': {
            target: process.env.API_ENDPOINT,
            pathRewrite: { '^/api/': '' }
        }
    },
    /*
     ** Nuxt Auth module configuration
     */
    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: {
                        url: '/api/auth/login',
                        method: 'post',
                        propertyName: 'token'
                    },
                    user: { url: '/api/user/me', method: 'get', propertyName: 'data' },
                    logout: { url: '/api/auth/logout', method: 'post' }
                }
            }
        },
        redirect: {
            home: false,
            logout: '/login',
            login: '/login'
                // callback: '/auth/login'
        }
    },
    /*
     ** Build configuration
     */
    build: {
        transpile: [/^element-ui/],
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {}
    }
}